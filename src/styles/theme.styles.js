const Theme = {
  colors: {
    primary: '#D60000',
    text: '#686670',
  },
  fonts: {
    primary: `"Opens Sans", sans-serif`,
    heading: `"Barlow Condensed", sans-serif`
  }
}

export default Theme