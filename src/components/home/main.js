import React from "react"
import Models from "./models";
import Ram from "./ram";
import Discover from "./discover";
import Dodge from "./dodge";
import HomeGrid from "./grid";
import About from "./about";

const HomeMain = () => (
    <main>
      <Models/>
      <Ram/>
      <Discover/>
      <Dodge/>
      <HomeGrid/>
      <About/>
    </main>
)

export default HomeMain