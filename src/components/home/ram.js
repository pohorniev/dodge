import React from "react"
import ModelsCard from "./models-card";
import { Title } from "../../styles/typography.styles";
import { Row, RamBlock } from "./home.styles";
import BtnLink from "../BtnLink";

const Ram = () => {
  return (
    <RamBlock>
      <div className={`container`}>
        <Title withBtn color={'white'}>RAM 1500</Title>
        <BtnLink url={'#'} label={'Odkryj RAM 1500'}/>

        <Row black>
          <ModelsCard black grid={5}
                      title="BIGHORN"
                      url="#"
                      image="/images/home/ram_bighorn.jpg"
                      color="white"/>
          <ModelsCard black grid={5}
                      title="REBEL" url="#"
                      image="/images/home/ram_rebel.jpg"
                      color="white"/>
          <ModelsCard black grid={5}
                      title="LARAMIE"
                      url="#"
                      image="/images/home/ram_laramie.jpg"
                      color="white"/>
          <ModelsCard black grid={5}
                      title="LONGHORN"
                      url="#"
                      image="/images/home/ram_longhorn.jpg"
                      color="white"/>
          <ModelsCard black grid={5}
                      title="LIMITED"
                      url="#"
                      image="/images/home/ram_limited.jpg"
                      color="white"/>
        </Row>
      </div>
    </RamBlock>
  )
}

export default Ram