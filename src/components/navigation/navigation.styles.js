import styled from "styled-components";
import { breakpoints } from "../../styles/breakpoints.styles";

export const NavContainer = styled.nav`
  width: 100%;
  height: 16rem;
  background-color: black;
  
  
  .container {
    display: flex;
    align-items: center;
    justify-content: space-between;
    height: 100%;
  }
  
  ${breakpoints('tabLand') `
    height: 8rem;
  `} 
`;

export const Logo = styled.div`
  img {
      width: 26rem;
      
      ${breakpoints('bigDesktop') `
        width: 32rem
      `} 
      
      ${breakpoints('tabLand') `
        width: 18rem
      `} 
      
      ${breakpoints('xs') `
        width: 14rem
      `} 
    }
    
    &.open {
      position: fixed;
      z-index: 11;
    }
`;

export const NavList = styled.ul`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
  list-style: none;
  width: 50%;
  height: 100%;
  
  ${breakpoints('tabLand') `
    display: none;
  `} 
  
  &.open {
    position: fixed;
    top: 0;
    left: 0;
    z-index: 10;
    width: 100vw;
    height: 100vh;
    background-color: black;
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
`;

export const NavItem = styled.li`
  //width: 24rem;
  height: 100%;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 2rem;
  
  ${breakpoints('tabLand') `
    height: auto;
    margin-right: 0;
    margin-bottom: 1rem;
  `} 
  
  &:hover > .submenu {
    opacity: 1;
    visibility: visible;
  }
  
  a {
    color: ${props => props.active ? props.theme.colors.primary : 'white'};
    font-family: ${props => props.theme.fonts.heading};
    font-size: 1.8rem;
    text-transform: uppercase;
    margin-right: 2rem;
    transition: color .3s;
    
    &:hover { 
      color: ${props => props.theme.colors.primary};
    };
    
    &:hover ~ .line {
      background-color: ${props => props.theme.colors.primary};
    }
  }
  
  .line {
    width: 4.4rem;
    height: 1px;
    background-color: ${props => props.active ? props.theme.colors.primary : 'white'};
    transition: background-color .3s;
    
    ${breakpoints('tabLand') `
      display: none;
    `} 
  }
`;

export const NavSubmenu = styled.div`
  display: flex;
  flex-direction: column;
  opacity: 0;
  visibility: hidden;
  background-color: rgba(0,0,0,0.86);
  border-top: 4px solid ${props => props.theme.colors.primary};
  position: absolute;
  top: 100%;
`;

export const NavSocial = styled.div`
  display: flex;
  flex-direction: column;
  
  ${breakpoints('tabLand') `
    flex-direction: row;
    height: 2.4rem;
  `} 
  
  a:not(:last-of-type) {
    width: 2.4rem;
    height: 2.4rem;
    margin-bottom: 1.6rem;
    
    ${breakpoints('tabLand') `
      margin-right: 1.6rem;
      margin-bottom: 0;
    `} 
    
    ${breakpoints('xs') `
      margin-right: 1.2rem;
    `} 
  }
`;

export const NavBtn = styled.div`
    width: 2.4rem;
    height: 2.4rem;
    display: none; 
    align-items: center;
    justify-content: center;
    margin-left: 1.6rem;
    
    &.open {
      position: fixed;
      right: 1.5rem;
      z-index: 11;
    }
    
    ${breakpoints('tabLand') `
      display: flex; 
    `} 
    
    ${breakpoints('xs') `
      margin-left: 1.2rem;
    `} 
`;

export const NavIcon = styled.div`
  position: relative;
  
  &,
  &:after,
  &:before {
    width: 2.4rem;
    height: 1px;
    background-color: #e4e4e4;
  }
  
  &:after,
  &:before {
    content: '';
    position: absolute;
  }
  
  &:after { top: -1rem;}
  &:before { top: 1rem;}
`;