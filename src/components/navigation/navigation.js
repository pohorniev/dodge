import React, {useState} from 'react'
import { Link } from 'gatsby'
import { NavContainer, Logo, NavList, NavItem, NavSubmenu, NavSocial, NavBtn, NavIcon } from "./navigation.styles";

const Nav = () => {
  const [openMenu, setOpenMenu] = useState(false)

  return (
      <NavContainer>
        <div className="container">
          <Logo className={openMenu ? `open` : ``}>
            <img src="/images/logo.svg" alt=""/>
          </Logo>

          <NavList className={openMenu ? `open` : ``}>
            <NavItem>
              <Link to={'/dodge'}>Dodge</Link>
              <div className="line"/>
            </NavItem>
            <NavItem>
              <Link to={'/ram'}>RAM</Link>
              <div className="line"/>

              <NavSubmenu className={`submenu`}>
                <Link to={'#'}>ram bighorn</Link>
                <Link to={'#'}>ram rebel</Link>
                <Link to={'#'}>ram laramie</Link>
                <Link to={'#'}>ram longhorn</Link>
                <Link to={'#'}>ram limited</Link>
              </NavSubmenu>
            </NavItem>
            <NavItem>
              <Link to={'/oferta'}>Oferta</Link>
              <div className="line"/>
            </NavItem>
            <NavItem>
              <Link to={'/salon'}>Salon</Link>
              <div className="line"/>
            </NavItem>
            <NavItem>
              <Link to={'/serwis'}>Serwis</Link>
              <div className="line"/>
            </NavItem>
            <NavItem>
              <Link to={'/kontakt'}>Kontakt</Link>
              <div className="line"/>
            </NavItem>
          </NavList>

          <NavSocial>
            <a href='#' target="_blank" rel="nofollow noreferrer" >
              <img src="/images/icons/icon-fb.svg" alt=""/>
            </a>
            <a href='#' target="_blank" rel="nofollow noreferrer">
              <img src="/images/icons/icon-instagram.svg" alt=""/>
            </a>
            <a href='#' target="_blank" rel="nofollow noreferrer">
              <img src="/images/icons/icon-rect.svg" alt=""/>
            </a>
            <NavBtn className={openMenu ? `open` : ``} onClick={() => setOpenMenu(!openMenu)}>
              <NavIcon/>
            </NavBtn>
          </NavSocial>

        </div>

      </NavContainer>
  )
}

export default Nav